#!/bin/bash

SCRIPT="dirSync.sh"
AUTHOR="Tristan Terpelle (tristan@terpelle.be)"
DATE="02/Jan/2013"
VERSION="0.09"
URL="https://bitbucket.org/tterpelle/dirsync.sh"

# Changelog

# v0.09:
#	- added -D (dry run) option
#	- lots of cleanups

#dirSync.sh is a script to synchronize one directory to another. The point is 
# to call it from udev so it will run when an external disk is inserted.
#
#The script takes a number of arguments with which it will take care of
#	1.  fsck'ing the partition on the external disk (if selected),
#	2.  mounting the partition,
#	3.  syncing the source directory to the target directory on the 
#           mounted partition,
#	4.  umounting the partition and finally
#	5.  clean up and print a bit of info to stdout or a mail.
#
#See this blog post on how to integrate it with udev to make it run 
#automatically when you insert a specific external disk: 
#http://tristan.terpelle.be/2012/11/automatic-synchronization-to-external.html

# ----- START CONFIGURATION ---------------------
# default options
# error status
_ERR="0"
# simulate execution and only echo what would be done? Leave empty for 'no', any other value 
# for 'yes'
_DRYRUN=""
# run fsck every time disk is inserted? Leave empty for 'no', any other value 
# for 'yes'
_FSCK=""
# who to send a mail to with script results
_MAILTO=""
# subject of mail to send when the script is done
_SUBJECT="dirSync result @ $(date)"
# location of the output file
_OUTPUTFILE=""

# ----- END CONFIGURATION -----------------------

# ----- SCRIPT FUNCTIONS ------------------------

execute() {
	if [ -n "${_DRYRUN}" ]
	then
		echo "${@}"
	else
		log "${@}"
		eval "${@}" || error "command \"${@}\" failed with return code $?"
	fi
}

# logging
log() {
	if [ -n "${_OUTPUTFILE}" ]
	then
		echo "$(date +%Y%m%d-%H%M%S): ${@}" >> ${_OUTPUTFILE}
	fi

	LOG="${LOG}\n$(date +%Y%m%d-%H%M%S): ${@}"
}

# send mail with script results
sendMail() {
	if [ -n "${_DRYRUN}" ]
	then
		echo "echo ${LOG} | mailx -s \"${_SUBJECT}\" ${_MAILTO}"
	else
		echo ${LOG} | mailx -s "${_SUBJECT}" ${_MAILTO}
	fi
}

# send mail with error report, clean up and exit with a failure
error() { 
	_REASON="${@}"
	_SUBJECT="${_SUBJECT} - ERROR: ${_REASON}"
	if [ -n "${_MAILTO}" ]
	then 
		sendMail
	else
		log "error: ${_REASON}"
		echo "error: ${_REASON}"
	fi

	if [ -z "${_QUIET}" ]
	then
		echo ${LOG}
	fi

	exit 1
}

# finish script 
finish() {
	_SUBJECT="${_SUBJECT} - SUCCESS"
	if [ -n "${_MAILTO}" ]
	then 
		sendMail
	fi

	if [ -n "${_OUTPUTFILE}" ]
	then
		echo "done: see ${_OUTPUTFILE}"
	fi

	if [ -z "${_QUIET}" ]
	then
		echo ${LOG}
	fi
	
	exit 0
}

# usage
usage() {
cat << EOF
Usage: $0 [-D] -d deviceName [-f] -m mountPoint -s sourceDir -t targetDir [-M emailAddress] [-q] [-o outputFile] [-h|-v]

This script
  1. runs fsck on deviceName (optionally)
  2. mounts deviceName on mountPoint
  3. synchronizes sourceDir to targetDir
  4. umounts deviceName
  5. sends a status mail to emailAddress (optionally)
       
 -D		  dry run, do not do anything, only print what would happen
 -d dev           disk device that will be synced to, e.g. /dev/sdc1
 -f               run fsck on inserted partition before syncing
 -h               help (all this)
 -m mountPoint    directory where the inserted partition will be mounted,
                  e.g. /media/sdc1
 -M emailAddress  address to send the email result to, if not set output will
                  be printed to stdout
 -o outputFile    log output to outputFile
 -q               quiet, no output to screen
 -s sourceDir     directory to synchronize to targetDir on the inserted
                  partition
 -t targetDir     directory under mountPoint where sourceDir will be synced to
 -v               version info
EOF
	exit 5
}

# version
version() {
cat << EOF
${SCRIPT} v${VERSION} (${DATE}), by ${AUTHOR}
EOF
	exit 10
}

# ----- END SCRIPT FUNCTIONS --------------------

# ----- SCRIPT START ----------------------------
# parse script options and parameters
while getopts "d:Dfhm:M:o:qs:t:v" opt
do
	case ${opt} in
		# device name of inserted partition, as passed by udev
		d) _DEV="${OPTARG}"
		;;
		
		# run in dry run mode
		D) _DRYRUN="yes"
		;;
		
		# run fsck on inserted partition before syncing
		f) _FSCK="yes"
		;;
		
		h) usage
		;;
		
		# label of target partition, as passed by udev
		m) _MOUNTPOINT="${OPTARG}"
		;;

		# email address to send the output file to
		M) _MAILTO="${OPTARG}"
		;;

		# temporary file to gather output
		o) _OUTPUTFILE="${OPTARG}"
		;; 

		# no output
		q) _QUIET="yes"
		;;
		
		# source directory for the rsync command to synchronize to the 
                # target, mind the trailing slash ('man rsync')
		s) _SOURCE="${OPTARG}"
		;;

		# target directory on the inserted partition for the rsync 
                # command, mind the trailing slash ('man rsync')
		t) _TARGET="${OPTARG}"
		;; 

		v) version
		;;
		
		*) usage
		;; 
	esac
done


# synchronization command to run
_RSYNC="/usr/bin/rsync -a --stats --delete ${_SOURCE} ${_MOUNTPOINT}/${_TARGET}"

if [ -n "${_OUTPUTFILE}" ]
then
	execute "touch ${_OUTPUTFILE}"
fi

log "Script ${0} started at $(date) on $(hostname)"
log "\$0=${0}"
log "args(${#})=\"${@}\""
log "_DEV=${_DEV}"
log "_DRYRUN=${_DRYRUN}"
log "_FSCK=${_FSCK}"
log "_MAILTO=${_MAILTO}"
log "_MOUNTPOINT=${_MOUNTPOINT}"
log "_OUTPUTFILE=${_OUTPUTFILE}"
log "_QUIET=${_QUIET}"
log "_RSYNC=${_RSYNC}"
log "_SOURCE=${_SOURCE}"
log "_SUBJECT=${_SUBJECT}"
log "_TARGET=${_TARGET}"

# check if script isn't running already
if [ $(pgrep -l $0) ]
then
	error "script ${0} already running"
fi 

# verify arguments passed to script
if [ -z "${_DEV}" ]
then
	error "missing parameter: -d deviceName"
elif [ -z "${_MOUNTPOINT}" ]
then
	error "missing parameter: -m mountPoint"
elif [ -z "${_SOURCE}" ]
then
	error "missing parameter: -s sourceDir"
elif [ -z "${_TARGET}" ]
then
	error "missing parameter: -t targetDir"
fi

# run fsck if configured
if [ -n "${_FSCK}" ]
then
	execute "fsck -n ${_DEV} &> /dev/null" 
fi

# mount backup partition
execute "mkdir -pv ${_MOUNTPOINT}"
execute "mount ${_DEV} ${_MOUNTPOINT}"
execute "df -m ${_MOUNTPOINT}"

# run sync
execute "${_RSYNC}"

# umount external disk and remove mount point, but make sure
# the moint point variable isn't empty, first
if [ -z "${_MOUNTPOINT}" ]
then
	error "refusing to 'rmdir ${_MOUNTPOINT}'"
else
	execute "umount ${_DEV}"
	execute "rmdir ${_MOUNTPOINT}"
fi

log "script finished at $(date)"
finish
exit 255
# ----- SCRIPT END ------------------------------
